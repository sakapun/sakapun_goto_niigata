import {Callout, Marker} from "react-native-maps";
import React, {RefObject, useEffect, useRef} from "react";
import {View, Text, Dimensions, Linking, Alert} from "react-native";
import {MapMarkerRefType} from "../App";
import {Store} from "../../../types/store";

type StorePinType = {
  store: Store,
  markerRef: RefObject<MapMarkerRefType>
}
export const StorePin = ({store, markerRef}: StorePinType, ) => {

  const thisRef = useRef<Marker | null>(null)
  useEffect(() => {
    if (thisRef.current && markerRef && markerRef.current) {
      markerRef.current.set(store.code, thisRef.current);
    }
  }, [thisRef])

  return (
    <Marker ref={thisRef} key={store.code} coordinate={{
      latitude: store.lat as number,
      longitude: store.lng as number
    }}>
      <Callout
        tooltip={false}
        onPress={() => {
          Alert.alert(
            "外部ブラウザで店名検索します",
            "よろしいですか",
            [
              {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
              },
              { text: "OK", onPress: () => {
                  Linking.openURL(
                    "https://www.google.com/search?q=" + store.name
                  ).catch(err => console.error('URLを開けませんでした。', err));
                }
              }
            ],
            { cancelable: false }
          )
        }}>
        <View style={{maxWidth: Dimensions.get('window').width * 0.8}}>
          <Text style={{fontWeight: "bold"}}>{store.name}</Text>
          <Text>{store.address}</Text>
          <Text>{store.genre}</Text>
        </View>
      </Callout>
    </Marker>
  )
}

