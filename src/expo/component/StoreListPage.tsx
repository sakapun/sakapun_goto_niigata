import {FlatList, Modal, SafeAreaView, Text, View} from "react-native";
import {ListItemStore} from "./ListItemStore";
import React, {useCallback, useMemo, useState} from "react";
import {Body, Container, Header, Icon, Left, Right, Title, Item, Input, Button, Form, List} from "native-base";
import {Store} from "../../../types/store";

export type StoreListPageType = {
  sortedStores: Store[],
  jumpAndCallout: (store: Store) => void,
  goBack: () => void
}
export const StoreListPage = React.memo<StoreListPageType>( (props: StoreListPageType) => {
  const {sortedStores, jumpAndCallout, goBack} = props

  const {handleChangeText, searchedStores} = useStoreList(sortedStores)
  const goBackCallback = useCallback(goBack, [])

  return (
    <Container>
      <Header searchBar rounded>
        <Left>
          <Icon
            type={"MaterialIcons"}
            name={"arrow-back"}
            onPress={goBackCallback}
          />
        </Left>
        <Body>
          <Title>
            店舗数({searchedStores.length})
            <Text style={{fontSize: 14}}> 登録日順</Text>
          </Title>
        </Body>
      </Header>
      <Body>
        <Form>
          <Item regular style={{width: "100%"}}>
            <Input placeholder="Search" onChangeText={handleChangeText} />
          </Item>
        </Form>
        <List
          dataArray={searchedStores}
          keyExtractor={(a: Store) => {
            return a.code
          }}
          renderItem={(data) => {
            return (
              <ListItemStore
                store={data.item}
                handleListClick={jumpAndCallout}
              />
            );
          }}
        />
      </Body>
    </Container>
  )
})

export function useStoreList (stores: Store[]) {
  const [searchText, setSearch] = useState<string>("")

  const handleChangeText = useCallback((text) => {
    setSearch(text)
  }, [])

  const searchedStores = useMemo(() => {
    return stores.filter(store => store.name.includes(searchText) || store.address.includes(searchText))
  }, [searchText, stores])

  return {
    handleChangeText,
    searchedStores
  }
}
