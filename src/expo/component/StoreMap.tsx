import {StorePin} from "./StorePin";
import MapView from "react-native-maps";
import React, {RefObject} from "react";
import {Store} from "../../../types/store";
import {Dimensions, StyleSheet} from "react-native";
import {City} from "../../data/cities";
import {MapMarkerRefType} from "../App";

export type StoreMapComponentType = {
  selectedCity: City,
  mapRef: RefObject<MapView>,
  markerRef: RefObject<MapMarkerRefType>,
  sortedStores: Store[],
}
export const StoreMap = React.memo<StoreMapComponentType>((props) => {
  const {selectedCity, mapRef, markerRef, sortedStores} = props;
  return (
    <MapView
      style={styles.mapStyle}
      initialRegion={selectedCity.region}
      showsUserLocation={true}
      showsMyLocationButton={true}
      showsCompass={true}
      followsUserLocation={true}
      showsScale={true}
      toolbarEnabled={true}
      ref={mapRef}
      provider={"google"}
    >
      {sortedStores.map((store) => {
        return (
          <StorePin markerRef={markerRef} key={store.code} store={store} />
        );
      })}
    </MapView>
  )
}, (prev, next) => {
  return prev.sortedStores.length === next.sortedStores.length
})

const styles = StyleSheet.create({
  mapStyle: {
    width: Dimensions.get("window").width,
    height: "100%",
    // height: Dimensions.get('window').height,
  },
});
