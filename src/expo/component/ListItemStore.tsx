import {Text} from "react-native";
import React, {useCallback} from "react";
import {ListItem, View} from "native-base";
import {yearMonthDay} from "../../lib/date_format";
import {Store} from "../../../types/store";


export type ListItemStoreType = {
  handleListClick: (store: Store) => void,
  store: Store
}
export const ListItemStore = (props : ListItemStoreType) => {
  const {store, handleListClick} = props;
  const handleClick = useCallback(() => {
    handleListClick(store);
  }, [store])

  return (
    <ListItem
      onPress={handleClick}
    >
      <View>
        <Text style={{fontWeight: "bold"}}>{store.name}</Text>
        <Text >{store.address}</Text>
        <Text >登録日：{yearMonthDay(store.updatedAt)}</Text>
      </View>
    </ListItem>
  );
}
