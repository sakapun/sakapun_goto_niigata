import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import { Modal } from "react-native";
import {
  Body,
  Container,
  Header,
  Icon,
  Left,
  ListItem,
  Right,
  Title,
  Text,
  Button,
  List,
} from "native-base";
import MapView, { Marker } from "react-native-maps";
import storeList from "../data/stores.json";

import * as Location from "expo-location";
import { LocationData } from "expo-location/src/Location";
import * as Font from "expo-font";
import { AppLoading } from "expo";
// import * as Analytics from "expo-firebase-analytics";

import { cities, City } from "../data/cities";
import { StoreListPage } from "./component/StoreListPage";
import { StoreMap } from "./component/StoreMap";

export type MapMarkerRefType = Map<string, Marker | null>;
export default function App() {
  const { isFontLoaded } = useInit();
  const [isModal, setModal] = useState<boolean>(false);
  const [isRegionModalOpen, setRegionModal] = useState<boolean>(false);

  const [selectedCity, setCity] = useState<City>(cities[0]);

  const mapRef = useRef<null | MapView>(null);
  const markerRef = useRef<MapMarkerRefType>(new Map());

  const jumpAndCallout = useCallback(
    (store) => {
      setModal(false);
      markerRef.current.get(store.code)!.showCallout();
      mapRef.current?.animateCamera({
        center: {
          latitude: store.lat,
          longitude: store.lng,
        },
        zoom: 16,
      });
    },
    [markerRef, mapRef]
  );

  const sortedStores = useMemo(() => {
    return storeList.sort((a, b) => {
      if (a.createdAt && b.createdAt) {
        return b.createdAt > a.createdAt ? 1 : -1;
      }
      return 1;
    });
  }, [storeList]);

  if (!isFontLoaded) {
    return <AppLoading />;
  }

  return (
    <Container>
      <Header searchBar={true}>
        <Left />
        <Body>
          <Title>{selectedCity.city.japanese}</Title>
          {/*<Title>*/}
          {/*  */}
          {/*</Title>*/}
          {/*<Button full transparent onPress={() => setRegionModal(true)}>*/}
          {/*  <Text style={{color: "black"}}>{selectedCity.city.japanese}</Text>*/}
          {/*</Button>*/}
        </Body>
        <Right>
          <Button transparent onPress={() => setRegionModal(true)}>
            <Icon name={"location-city"} type={"MaterialIcons"} />
          </Button>
          <Button transparent onPress={() => setModal(true)}>
            <Icon name={"list"} type={"MaterialIcons"} />
          </Button>
        </Right>
      </Header>
      <Body>
        <StoreMap
          sortedStores={sortedStores}
          mapRef={mapRef}
          markerRef={markerRef}
          selectedCity={selectedCity}
        />
      </Body>
      <Modal
        visible={isModal}
        animationType={"slide"}
        presentationStyle={"fullScreen"}
      >
        <StoreListPage
          sortedStores={sortedStores}
          jumpAndCallout={jumpAndCallout}
          goBack={() => setModal(false)}
        />
      </Modal>
      <Modal visible={isRegionModalOpen} animationType={"fade"}>
        <Container>
          <Header searchBar={true}>
            <Left>
              <Icon
                type={"MaterialIcons"}
                name={"arrow-back"}
                onPress={() => setRegionModal(false)}
              />
            </Left>
            <Body>
              <Title>地域の選択</Title>
            </Body>
            <Right>
              {/*<Icon name={"notifications-active"} type={"MaterialIcons"} />*/}
            </Right>
          </Header>
          <Body>
            <List
              dataArray={cities}
              renderItem={(data) => {
                const city = data.item;

                return (
                  <ListItem
                    onPress={() => {
                      setCity(city);
                      setRegionModal(false);
                      mapRef.current?.animateToRegion(city.region);
                    }}
                  >
                    <Text style={{ width: "100%" }}>{city.city.japanese}</Text>
                  </ListItem>
                );
              }}
              keyExtractor={(item) => "" + item.cityId}
            />
          </Body>
        </Container>
      </Modal>
    </Container>
  );
}

export function useInit() {
  const [location, setLocation] = useState<LocationData | null>(null);
  const [errorMsg, setErrorMsg] = useState<string>("");
  const [isFontLoaded, setFontLoaded] = useState<boolean>(false);

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestPermissionsAsync();
      if (status !== "granted") {
        setErrorMsg("Permission to access location was denied");
      }

      let location = await Location.getCurrentPositionAsync({});
      setLocation(location);
    })();
  }, []);

  useEffect(() => {
    (async () => {
      await Font.loadAsync({
        Roboto: require("native-base/Fonts/Roboto.ttf"),
        Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
        Ionicons: require("@expo/vector-icons/build/vendor/react-native-vector-icons/Fonts/Ionicons.ttf"),
        MaterialIcons: require("@expo/vector-icons/build/vendor/react-native-vector-icons/Fonts/MaterialIcons.ttf"),
        "Material Icons": require("@expo/vector-icons/build/vendor/react-native-vector-icons/Fonts/MaterialIcons.ttf"),
      });
      setFontLoaded(true);
    })();
  }, []);

  // analytics
  // useEffect(() => {
  //   (async () => {
  //     await Analytics.logEvent("uhouho", {
  //       "donkey": 9
  //     })
  //   })()
  // }, [])

  return { location, errorMsg, isFontLoaded };
}
