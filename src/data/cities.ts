import { Region } from "react-native-maps";

export type City = {
  cityId: number;
  city: MyLocation;
  pref: MyLocation;
  region: Region;
  searchPoint: string[];
};
export type MyLocation = {
  latin: string;
  japanese: string;
};

export const cities: [
  City,
  City,
  City,
  City,
  City,
  City,
  City,
  City,
  City,
  City,
  City,
  City,
  City,
  City,
  City,
  City,
  City,
  City,
  City
] = [
  {
    cityId: 2148,
    city: {
      latin: "Niigata",
      japanese: "新潟市",
    },
    pref: {
      latin: "Niigata",
      japanese: "新潟県",
    },
    region: {
      latitude: 37.913209,
      longitude: 139.058941,
      latitudeDelta: 0.146,
      longitudeDelta: 0.126,
    },
    searchPoint: [],
  },
  {
    cityId: 2139,
    city: {
      latin: "Nagaoka",
      japanese: "長岡市",
    },
    pref: {
      latin: "Niigata",
      japanese: "新潟県",
    },
    region: {
      latitude: 37.446265,
      longitude: 138.851277,
      latitudeDelta: 0.105,
      longitudeDelta: 0.03,
    },
    searchPoint: [],
  },
  {
    cityId: 2145,
    city: {
      japanese: "三条市",
      latin: "Sanjo",
    },
    region: {
      latitude: 37.63676,
      longitude: 138.961689,
      latitudeDelta: 0.076,
      longitudeDelta: 0.026,
    },
    pref: {
      japanese: "新潟県",
      latin: "Niigata",
    },
    searchPoint: [],
  },
  {
    cityId: 2184,
    city: {
      japanese: "上越市",
      latin: "Joetsu",
    },
    pref: {
      japanese: "新潟県",
      latin: "Niigata",
    },
    region: {
      latitude: 37.147848,
      longitude: 138.23608,
      latitudeDelta: 0.066,
      longitudeDelta: 0.096,
    },
    searchPoint: [],
  },
  {
    cityId: 2147,
    region: {
      latitude: 37.3719,
      longitude: 138.558984,
      latitudeDelta: 0.046,
      longitudeDelta: 0.026,
    },
    city: {
      japanese: "柏崎市",
      latin: "Kashiwazaki",
    },
    pref: {
      latin: "Niigata",
      japanese: "新潟券",
    },
    searchPoint: [],
  },
  {
    cityId: 2150,
    region: {
      latitude: 37.950886,
      longitude: 139.327892,
      latitudeDelta: 0.046,
      longitudeDelta: 0.026,
    },
    city: {
      japanese: "新発田市",
      latin: "Shibata",
    },
    pref: {
      latin: "Niigata",
      japanese: "新潟県",
    },
    searchPoint: [],
  },
  {
    cityId: 2183,
    region: {
      latitude: 37.314339,
      longitude: 138.7951,
      latitudeDelta: 0.046,
      longitudeDelta: 0.026,
    },
    city: {
      japanese: "小千谷市",
      latin: "Ojiya",
    },
    pref: {
      latin: "Niigata",
      japanese: "新潟県",
    },
    searchPoint: [],
  },
  {
    cityId: 2185,
    region: {
      latitude: 37.666275,
      longitude: 139.040198,
      latitudeDelta: 0.046,
      longitudeDelta: 0.026,
    },
    city: {
      japanese: "加茂市",
      latin: "Kamo",
    },
    pref: {
      latin: "Niigata",
      japanese: "新潟県",
    },
    searchPoint: [],
  },
  {
    cityId: 12189,
    region: {
      latitude: 37.127582,
      longitude: 138.755708,
      latitudeDelta: 0.046,
      longitudeDelta: 0.026,
    },
    city: {
      japanese: "十日町市",
      latin: "Tokamachi",
    },
    pref: {
      latin: "Niigata",
      japanese: "新潟県",
    },
    searchPoint: [],
  },
  {
    cityId: 22189,
    region: {
      latitude: 38.223979,
      longitude: 139.480021,
      latitudeDelta: 0.046,
      longitudeDelta: 0.026,
    },
    city: {
      japanese: "村上市",
      latin: "Murakami",
    },
    pref: {
      latin: "Niigata",
      japanese: "新潟県",
    },
    searchPoint: [],
  },
  {
    cityId: 32189,
    region: {
      latitude: 37.673152,
      longitude: 138.88223,
      latitudeDelta: 0.046,
      longitudeDelta: 0.026,
    },
    city: {
      japanese: "燕市",
      latin: "Tsubame",
    },
    pref: {
      latin: "Niigata",
      japanese: "新潟県",
    },
    searchPoint: [],
  },
  {
    cityId: 42189,
    region: {
      latitude: 37.039028,
      longitude: 137.862652,
      latitudeDelta: 0.046,
      longitudeDelta: 0.026,
    },
    city: {
      japanese: "糸魚川市",
      latin: "Itoigawa",
    },
    pref: {
      latin: "Niigata",
      japanese: "新潟県",
    },
    searchPoint: [],
  },
  {
    cityId: 52189,
    region: {
      latitude: 37.025196,
      longitude: 138.253473,
      latitudeDelta: 0.076,
      longitudeDelta: 0.086,
    },
    city: {
      japanese: "妙高市",
      latin: "Myoko",
    },
    pref: {
      latin: "Niigata",
      japanese: "新潟県",
    },
    searchPoint: [],
  },
  {
    cityId: 62189,
    region: {
      latitude: 37.744536,
      longitude: 139.182568,
      latitudeDelta: 0.046,
      longitudeDelta: 0.026,
    },
    city: {
      japanese: "五泉市",
      latin: "Gosen",
    },
    pref: {
      latin: "Niigata",
      japanese: "新潟県",
    },
    searchPoint: [],
  },
  {
    cityId: 72189,
    region: {
      latitude: 37.834501,
      longitude: 139.225983,
      latitudeDelta: 0.046,
      longitudeDelta: 0.026,
    },
    city: {
      japanese: "阿賀野市",
      latin: "Agano",
    },
    pref: {
      latin: "Niigata",
      japanese: "新潟県",
    },
    searchPoint: [],
  },
  {
    cityId: 82189,
    region: {
      latitude: 38.018353,
      longitude: 138.368082,
      latitudeDelta: 0.246,
      longitudeDelta: 0.226,
    },
    city: {
      japanese: "佐渡市",
      latin: "Sado",
    },
    pref: {
      latin: "Niigata",
      japanese: "新潟県",
    },
    searchPoint: [],
  },
  {
    cityId: 112189,
    region: {
      latitude: 37.244124,
      longitude: 138.924361,
      latitudeDelta: 0.046,
      longitudeDelta: 0.026,
    },
    city: {
      japanese: "魚沼市",
      latin: "Uonuma",
    },
    pref: {
      latin: "Niigata",
      japanese: "新潟県",
    },
    searchPoint: [],
  },
  {
    cityId: 122189,
    region: {
      latitude: 37.161428,
      longitude: 138.925448,
      latitudeDelta: 0.046,
      longitudeDelta: 0.026,
    },
    city: {
      japanese: "南魚沼市",
      latin: "Minamiuonuma",
    },
    pref: {
      latin: "Niigata",
      japanese: "新潟県",
    },
    searchPoint: [],
  },
  {
    cityId: 132189,
    region: {
      latitude: 36.934025,
      longitude: 138.81744,
      latitudeDelta: 0.046,
      longitudeDelta: 0.026,
    },
    city: {
      japanese: "湯沢町",
      latin: "Yuzawa",
    },
    pref: {
      latin: "Niigata",
      japanese: "新潟県",
    },
    searchPoint: [],
  },
];
