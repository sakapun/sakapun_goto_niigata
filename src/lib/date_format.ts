export function yearMonthDay(s: string | null): string {
  if (!s) {
    return "";
  }
  const date = new Date(s);
  return (
    date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate()
  );
}
