/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createGotoStore = /* GraphQL */ `
  mutation CreateGotoStore(
    $input: CreateGotoStoreInput!
    $condition: ModelGotoStoreConditionInput
  ) {
    createGotoStore(input: $input, condition: $condition) {
      id
      code
      genre
      name
      address
      tel
      togo
      delivery
      _version
      _deleted
      _lastChangedAt
    }
  }
`;
export const updateGotoStore = /* GraphQL */ `
  mutation UpdateGotoStore(
    $input: UpdateGotoStoreInput!
    $condition: ModelGotoStoreConditionInput
  ) {
    updateGotoStore(input: $input, condition: $condition) {
      id
      code
      genre
      name
      address
      tel
      togo
      delivery
      _version
      _deleted
      _lastChangedAt
    }
  }
`;
export const deleteGotoStore = /* GraphQL */ `
  mutation DeleteGotoStore(
    $input: DeleteGotoStoreInput!
    $condition: ModelGotoStoreConditionInput
  ) {
    deleteGotoStore(input: $input, condition: $condition) {
      id
      code
      genre
      name
      address
      tel
      togo
      delivery
      _version
      _deleted
      _lastChangedAt
    }
  }
`;
