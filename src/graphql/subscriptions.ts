/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateGotoStore = /* GraphQL */ `
  subscription OnCreateGotoStore {
    onCreateGotoStore {
      id
      code
      genre
      name
      address
      tel
      togo
      delivery
      _version
      _deleted
      _lastChangedAt
    }
  }
`;
export const onUpdateGotoStore = /* GraphQL */ `
  subscription OnUpdateGotoStore {
    onUpdateGotoStore {
      id
      code
      genre
      name
      address
      tel
      togo
      delivery
      _version
      _deleted
      _lastChangedAt
    }
  }
`;
export const onDeleteGotoStore = /* GraphQL */ `
  subscription OnDeleteGotoStore {
    onDeleteGotoStore {
      id
      code
      genre
      name
      address
      tel
      togo
      delivery
      _version
      _deleted
      _lastChangedAt
    }
  }
`;
