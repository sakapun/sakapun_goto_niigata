/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const syncGotoStores = /* GraphQL */ `
  query SyncGotoStores(
    $filter: ModelGotoStoreFilterInput
    $limit: Int
    $nextToken: String
    $lastSync: AWSTimestamp
  ) {
    syncGotoStores(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      lastSync: $lastSync
    ) {
      items {
        id
        code
        genre
        name
        address
        tel
        togo
        delivery
        _version
        _deleted
        _lastChangedAt
      }
      nextToken
      startedAt
    }
  }
`;
export const getGotoStore = /* GraphQL */ `
  query GetGotoStore($id: ID!) {
    getGotoStore(id: $id) {
      id
      code
      genre
      name
      address
      tel
      togo
      delivery
      _version
      _deleted
      _lastChangedAt
    }
  }
`;
export const listGotoStores = /* GraphQL */ `
  query ListGotoStores(
    $filter: ModelGotoStoreFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listGotoStores(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        code
        genre
        name
        address
        tel
        togo
        delivery
        _version
        _deleted
        _lastChangedAt
      }
      nextToken
      startedAt
    }
  }
`;
