import React, {RefObject, useCallback} from "react";
import {ListItem} from "@chakra-ui/core/dist";
import { Map } from 'react-leaflet'

export type ShopNameLabelType = {
  title: string,
  lat: number,
  lng: number,
  refOb: RefObject<Map>
}
export const ShopNameLabel = (props: ShopNameLabelType) => {
  const {lat, lng, refOb} = props;
  const onClick = useCallback(() => {
    if(refOb && refOb.current) {
      refOb.current.leafletElement.panTo([lat, lng])
    }
  }, [refOb, lat, lng])
  return (
    <ListItem onClick={onClick}>
      {props.title}
    </ListItem>
  )
}
