import p from './p2.json';
import {StorePDF, GeocoderResponse, Store} from '../types/store';
import axios from 'axios';
import * as fs from "fs";
import storenow from "../src/data/stores.json";
import GeocoderResult = google.maps.GeocoderResult;
require("dotenv").config()


function pdfjsonToStores (): StorePDF[] {
  const lines = p.map(page => page.data).flat();
  return lines.reduce((sArr: StorePDF[], [code, genre, name, address, tel, togo, delivery], index, all) => {
    if (code.text.match(/\d\d\d\d\d/)) {
      sArr.push({
        code: code.text,
        genre: genre.text,
        name: name.text.replace(/\r/g, ''),
        address: address.text.replace(/\r/g, ''),
        tel: tel.text,
        togo: togo.text === '有り',
        delivery: delivery.text === '有り',
      });
    }
    return sArr;
  }, []);
}

async function geocodedStores (storesPDF: StorePDF[]): Promise<Store[]> {
  return await Promise.all(storesPDF.map<Promise<Store>>(async (store) => {
      // 名前と住所で検索して正確性を高めるが、それでもだめなら住所で検索する
      const [formattedAddress, lat, lng] =
      await geocodeRequest(store.name, store.address)
        || await geocodeRequest("", store.address)
        || [null, null, null]
      return {
        ...store,
        lat,
        lng,
        formattedAddress,
        createdAt: (new Date()).toString(),
        updatedAt: (new Date()).toString(),
      }
    }))
}

type GeoReqResp = [string, number , number]
async function geocodeRequest (name: string, address: string): Promise<GeoReqResp | null> {
  const nonSpaceName = name.replace(" ", "")
  const nonSpaceAddress = address.replace(" ", "")
  const requestStr = encodeURI( nonSpaceAddress + " " + nonSpaceName )
  return await axios.get<GeocoderResponse>(`https://maps.googleapis.com/maps/api/geocode/json?language=ja&address=${requestStr}&key=${process.env.GOOGLE_MAP_KEY}&region=jp`)
    .then(res => {
      const result: GeocoderResult = res.data.results[0]
      if (!result) {
        return null;
      }
      return [
        result.formatted_address,
        result.geometry.location.lat as unknown as number,
        result.geometry.location.lng as unknown as number,
      ];
    });
}

// データの上書きには重複排除（Map）を使った。検索データの上書き用関数
async function uwagakiScript() {
  const updatedStores = storenow.filter(store => {
    return [
      "00700",
    ].includes(store.code)
  })
  // console.log(updatedStores)

  const newStores: Store[] = await geocodedStores(updatedStores.slice(0, 30))
  const asStore = storenow as Store[]
  const mapped = asStore.concat(newStores).reduce((retMap: Map<string, Store>, store) => {
    retMap.set(store.code, store)
    return retMap
  }, new Map());
  console.log(newStores, storenow.length, mapped.size)

  fs.writeFileSync('src/data/stores.json', JSON.stringify(Array.from(mapped.values()), null, '  '))
}
// uwagakiScript()

// 重複排除で、latlng有りを残すオペレーション
function duplicateRemove () {
  const stores: Map<string, Store> = storenow.reduce((s: Map<string, Store>, store) => {
    const found = s.get(store.code)
    if (found) {
      if (!found.formattedAddress && store.formattedAddress ) {
        s.set(store.code, store)
      }
    } else {
      s.set(store.code, store)
    }
    return s;
  }, new Map())
  const arstore = Array.from(stores.values())

  fs.writeFileSync('src/data/stores2.json', JSON.stringify(arstore, null, '  '))
}

// 通常のデータ追加処理。普通に追加でよろしいかと
async function main() {
  const stores = pdfjsonToStores();
  const updatedStores = stores.filter(store => {
    return !storenow.find(s => s.code === store.code)
  })
  console.log(updatedStores.length)
  return

  const newStores: Store[] = await geocodedStores(updatedStores)

  const joinedStores: Store[] = newStores.concat(storenow)

  fs.writeFileSync('src/data/stores.json', JSON.stringify(joinedStores, null, '  '))
}

main()
