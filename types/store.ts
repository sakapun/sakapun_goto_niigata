import GeocoderStatus = google.maps.GeocoderStatus;
import GeocoderResult = google.maps.GeocoderResult;

export type StorePDF = {
  code: string,
  genre: string,
  name: string,
  address: string,
  tel: string,
  togo: boolean,
  delivery: boolean,
}

export type Store = StorePDF & {
  lat: number | null,
  lng: number | null,
  formattedAddress: string | null,
  createdAt: string,
  updatedAt: string,
}

export type GeocoderResponse = {
  results: GeocoderResult[],
  status: GeocoderStatus
}
