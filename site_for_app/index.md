# GotoEat 新潟 Map

- 本アプリはiOS、及びAndroidにのみ対応しております。
- アプリをダウンロードしてご利用ください
  
### 開発者情報
開発者：Sakapun  
連絡先：[@sakapun](https://twitter.com/sakapun)
  
  
#### プライバシーポリシー  
[こちらのページへ](./privacy)  

